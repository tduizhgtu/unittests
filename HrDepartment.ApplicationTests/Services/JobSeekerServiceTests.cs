﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HrDepartment.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using HrDepartment.Infrastructure.Interfaces;
using HrDepartment.Application.Interfaces;
using HrDepartment.Domain.Entities;
using AutoMapper;

namespace HrDepartment.Application.Services.Tests
{
    [TestClass()]
    public class JobSeekerServiceTests
    {
        [TestMethod()]
        public void RateJobSeekerAsyncTest_GetId1_ReturnRating95()
        {
            JobSeeker ArtTom = new JobSeeker();

            var mockStorage = new Mock<IStorage>();
            var mockRateService = new Mock<IRateService<JobSeeker>>();
            var mockNotificationService = new Mock<INotificationService>();
            var mockMapper = new Mock<IMapper>();

            mockStorage.Setup(d => d.GetByIdAsync<JobSeeker, int>(1)).ReturnsAsync(ArtTom);
            mockRateService.Setup(d => d.CalculateJobSeekerRatingAsync(ArtTom)).ReturnsAsync(95);

            var jobSC = new JobSeekerService(mockStorage.Object, mockRateService.Object, mockMapper.Object, mockNotificationService.Object);

            var actual = jobSC.RateJobSeekerAsync(1);
            var expected = 95;

            Assert.AreEqual(expected, actual.Result);
        }

        [TestMethod()]
        public void RateJobSeekerAsyncTest_GetId2_ReturnRating100()
        {
            JobSeeker ArtTom = new JobSeeker();

            var mockStorage = new Mock<IStorage>();
            var mockRateService = new Mock<IRateService<JobSeeker>>();
            var mockNotificationService = new Mock<INotificationService>();
            var mockMapper = new Mock<IMapper>();

            mockStorage.Setup(d => d.GetByIdAsync<JobSeeker, int>(1)).ReturnsAsync(ArtTom);
            mockRateService.Setup(d => d.CalculateJobSeekerRatingAsync(ArtTom)).ReturnsAsync(100);
            mockNotificationService.Setup(d => d.NotifyRockStarFoundAsync(ArtTom));

            var jobSC = new JobSeekerService(mockStorage.Object, mockRateService.Object, mockMapper.Object, mockNotificationService.Object);

            var actual = jobSC.RateJobSeekerAsync(1);
            var expected = 100;

            Assert.AreEqual(expected, actual.Result);
        }
    }
}