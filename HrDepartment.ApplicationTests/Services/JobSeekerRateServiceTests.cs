﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HrDepartment.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using HrDepartment.Domain.Entities;
using HrDepartment.Application.Interfaces;
using System.Threading.Tasks;
using Moq;

namespace HrDepartment.Application.Services.Tests
{
    [TestClass()]
    public class JobSeekerRateServiceTests
    {

        [TestMethod()]
		public void Ctor_SanctionsServiceIsNull_ThrowsArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new JobSeekerRateService(null));
        }

        [TestMethod()]
        public async Task CalculateJobSeeker_OtherEducation_ThrowsArgumentOutOfRangeException()
        {

            JobSeeker ArtTom = new JobSeeker() { Id = 6, LastName = "Tomilov", FirstName = "Artem", MiddleName = "V.", BirthDate = new DateTime(1991, 5, 15), Education = (Domain.Enums.EducationLevel)5 , Status = Domain.Enums.JobSeekerStatus.InProgress, Experience = 0.5, BadHabits = Domain.Enums.BadHabits.Drugs };

            var mock = new Mock<ISanctionService>();

            var rating = new JobSeekerRateService(mock.Object);

            Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() =>rating.CalculateJobSeekerRatingAsync(ArtTom));

        }

        [TestMethod()]
        public async Task CalculateJobSeeker_JobSeekerIsInSanctionList_ReturnRatingZero()
        {

            JobSeeker ArtTom = new JobSeeker() { Id = 7, LastName = "Tomilov", FirstName = "Artem", MiddleName = "V.", BirthDate = new DateTime(1991, 5, 15), Education = Domain.Enums.EducationLevel.University, Status = Domain.Enums.JobSeekerStatus.InProgress, Experience = 11, BadHabits = Domain.Enums.BadHabits.None };

            var mock = new Mock<ISanctionService>();

            mock.Setup(d => d.IsInSanctionsListAsync("Tomilov", "Artem", "V.", new DateTime(1991, 5, 15))).ReturnsAsync(true);

            var rating = new JobSeekerRateService(mock.Object);
            var actual = rating.CalculateJobSeekerRatingAsync(ArtTom);

            int expected = 0;

            Assert.AreEqual(expected, actual.Result);

        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void Test(JobSeeker ArtTom, int expected)
        {
            var mock = new Mock<ISanctionService>();

            var rating = new JobSeekerRateService(mock.Object);
            var actual = rating.CalculateJobSeekerRatingAsync(ArtTom);


            Assert.AreEqual(expected, actual.Result);
        }

        public static IEnumerable<object[]> GetData()
        {
            return new List<object[]>()
            {
                new object[] {new JobSeeker { Id = 1, LastName = "Tomilov", FirstName = "Artem", MiddleName = "V.", BirthDate = new DateTime(1991, 5, 15), Education = Domain.Enums.EducationLevel.University, Status = Domain.Enums.JobSeekerStatus.InProgress, Experience = 11, BadHabits = Domain.Enums.BadHabits.None }, 95 },
                new object[] {new JobSeeker { Id = 2, LastName = "Tomilov", FirstName = "Artem", MiddleName = "V.", BirthDate = new DateTime(2005, 5, 15), Education = Domain.Enums.EducationLevel.College, Status = Domain.Enums.JobSeekerStatus.InProgress, Experience = 9, BadHabits = Domain.Enums.BadHabits.Smoking }, 45 },
                new object[] {new JobSeeker { Id = 3, LastName = "Tomilov", FirstName = "Artem", MiddleName = "V.", BirthDate = new DateTime(1945, 5, 15), Education = Domain.Enums.EducationLevel.School, Status = Domain.Enums.JobSeekerStatus.InProgress, Experience = 4, BadHabits = Domain.Enums.BadHabits.Alcoholism }, 15 },
                new object[] {new JobSeeker { Id = 4, LastName = "Tomilov", FirstName = "Artem", MiddleName = "V.", BirthDate = new DateTime(1945, 5, 15), Education = Domain.Enums.EducationLevel.School, Status = Domain.Enums.JobSeekerStatus.InProgress, Experience = 2, BadHabits = Domain.Enums.BadHabits.Alcoholism }, 10 },
                new object[] {new JobSeeker { Id = 5, LastName = "Tomilov", FirstName = "Artem", MiddleName = "V.", BirthDate = new DateTime(1991, 5, 15), Education = Domain.Enums.EducationLevel.None, Status = Domain.Enums.JobSeekerStatus.InProgress, Experience = 0.5, BadHabits = Domain.Enums.BadHabits.Drugs }, 0 }
            };
        }
    }
}


